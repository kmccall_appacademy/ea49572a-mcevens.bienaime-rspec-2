def reverser()
    sentense = yield
    arr_sentense = sentense.split(" ")
    arr_sentense.each_with_index do |el,idx|
       arr_sentense[idx] = reverse_word(el)
    end
    arr_sentense.join(" ")
end

def reverse_word(word)
    word.split("").reverse.join("")
end

def adder(var = 1)
    yield + var
end

def repeater(count = 1)
   i = 0
   while i < count
     yield
     i+=1
   end
end
