require "time"


def measure(n = 1)

i = 0
arr_time = []
while i < n
  start_time = Time.now
  yield
  end_time = Time.now
  arr_time.push(end_time - start_time)
  i+=1
end

arr_time.reduce(:+) / n

end
